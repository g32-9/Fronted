import { Route, Routes } from "react-router";
import Login from "../components/Login";
import RecibirProps from "../components/RecibirProps"

export const UnauthRouter = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Login/>}></Route>
                    <Route path="recibirprops" element={<RecibirProps/>}></Route>
            </Routes>
        </>
    );
}
