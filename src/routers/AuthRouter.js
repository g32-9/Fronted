import { Route, Routes } from "react-router";

import Dashboard from "../components/Dashboard";
import MiPrimerComponente from "../components/MiPrimerComponente";


export const AuthRouter = () => {
    return (
        <>
            <Routes>
                <Route path="/" element={<Dashboard />}></Route>
                    <Route index element={<MiPrimerComponente/>}></Route>
            </Routes>
        </>
    );
}