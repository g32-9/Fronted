//imr + enter me genera:
import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function MiPrimerComponente() {
    const [correo, setCorreo]= useState("");
    const [pass, setPass]= useState("");
    const handleCorreo = (e)=> setCorreo(e.target.value);
    const handlePass = (e)=> setPass(e.target.value);
    return (
        <>
            <h1>Mi primer componente</h1>
            {/*Formulario login*/}
            <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control value={correo} type="email" onChange={handleCorreo} placeholder="Enter email" />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control value={pass} type="password" onChange={handlePass} placeholder="Password" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        </>
    );
}